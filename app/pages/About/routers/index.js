import React from 'react';
import AboutPage from '../index';
import {Route} from 'react-router-dom';
import {GLOBAL} from '../../../config/index';

export default<Route exact component={AboutPage} path={GLOBAL.createPathLocale(AboutPage.Path)}/>;
