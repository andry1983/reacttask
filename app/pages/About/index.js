import './style/index.scss';
import React, { Component} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import is from 'is_js';
import {GLOBAL} from '../../config/index';

class AboutPage extends Component {
    static Path = '/about';
    pageName = 'about';
    updateSwitch = true;
    constructor(props) {
        super(props);
        let state = {
            showMenu: false
        };
        this.state = state;
    }
    componentWillMount() {
        let {dispatch} = this.props;
        let lang = GLOBAL.getPathForUrl('lang');
        if (!GLOBAL.localeSwitch(dispatch, lang)) {
            console.log('error change localeSwitch ' + this.pageName );
        }
        if (!GLOBAL.changePageName(dispatch, this.pageName)) {
            console.log('error change pageName ' + this.pageName );
        }
        this.updateSwitch = false;
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (!this.updateSwitch) {
            this.updateSwitch = true;
            return false;
        }
        return true;
    }
    render() {
        let {locale, switcherMenu} = this.props;
        if (is.undefined(locale) || !locale ) {
            let {getLocalePage} = GLOBAL;
            locale = getLocalePage('HomePageLocale');
        }
        return (
            !switcherMenu
            ?
            <div id="bodyAbout">
                <h1>About</h1>
                <Link to={GLOBAL.createPathLocale('/')}>home</Link>
            </div>
            :
            <div>
                <p>Aboute animate</p>
            </div>
        );
    }
}

AboutPage.PropTypes = {
    listMenu: PropTypes.array.isRequired
};

function mapStateToProps(state) {
    return {
        locale: state.langPage.AboutPageLocale,
        switcherMenu: state.switcherMenu
    };
}

export default connect(mapStateToProps)(AboutPage);
