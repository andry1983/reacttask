import React from 'react';
import ErrorPage from '../index';
import {Route} from 'react-router-dom';

export default (<Route exact component={ErrorPage} path={ErrorPage.Path}/>);
