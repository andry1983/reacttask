import '../allStyle/index.scss';
import React, { Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import BodyContainer from '../../components';
import is from 'is_js';
import {GLOBAL} from '../../config/index';

class ErrorPage extends Component {
    static Path = '*';
    pageName = '404';
    updateSwitch = true;
    constructor(props) {
        super(props);
        let state = {
            showMenu: false
        };
        GLOBAL.createPathLocale(this.Path);
        this.state = state;
    }
    componentWillMount() {
        let {dispatch} = this.props;
        let lang = GLOBAL.getPathForUrl('lang');
        if (!GLOBAL.localeSwitch(dispatch, lang)) {
            console.log('error change pageName ' + this.pageName );
        }
        if (!GLOBAL.changePageName(dispatch, this.pageName)) {
            console.log('error change pageName ' + this.pageName );
        }
        this.updateSwitch = false;
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (!this.updateSwitch) {
            this.updateSwitch = true;
            return false;
        }
        return true;
    }
    setParamBody() {
        let bodyParam = ::this.getParamBody();
    }
    getCenterContent() {
        return (
            <h1>404 not found</h1>
        );
    }
    getParamBody() {
        let headerParam = {
            visability: true,
            iconStatus: true,
            iconStyle: 'standart',
            menuStatus: true,
            activeLink: this.pageName
        };
        let LeftContainerParam = {
            visability: true
        };
        let CenterContainerParam = {
            visability: true,
            child: (::this.getCenterContent())
        };
        let RightContainerParam = {
            visability: true
        };
        let FooterParam = {
            visability: true,
            menuStatus: true,
            activeLink: this.pageName
        };
        let paramsBody = {
            headerParam,
            LeftContainerParam,
            CenterContainerParam,
            RightContainerParam,
            FooterParam
        };
        return paramsBody;
    }
    render() {
        let paramsBody = ::this.getParamBody();
        console.log('ErrorPage', this.props);
        let path = GLOBAL.getPathProps(this.props);
        console.log('path', path);
        return (
            <div className="containerPage">
                <BodyContainer
                    {...paramsBody}
                />
            </div>
        );
    }
}

ErrorPage.PropTypes = {
    listMenu: PropTypes.array.isRequired
};

function mapStateToProps(state) {
    return {
        locale: state.langPage.ErrorPage
    };
}
export default connect(mapStateToProps)(ErrorPage);
