import '../allStyle/index.scss';
import React, { Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import BodyContainer from '../../components';
import is from 'is_js';
import {GLOBAL} from '../../config/index';
import {Link} from 'react-router-dom';

class HomePage extends Component {
    static Path = '/';
    pageName = 'home';
    updateSwitch = true;
    constructor(props) {
        super(props);
        // this.Path = GLOBAL.createPathLocale(this.pageName);
        this.state = {
            showMenu: false
        };
    }
    componentWillMount() {
        let {dispatch} = this.props;
        let lang = GLOBAL.getPathForUrl('lang');
        if (!GLOBAL.localeSwitch(dispatch, lang)) {
            console.log('error change localeSwitch ' + this.pageName );
        }
        if (!GLOBAL.changePageName(dispatch, this.pageName)) {
            console.log('error change pageName ' + this.pageName );
        }
        this.updateSwitch = false;
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (!this.updateSwitch) {
            this.updateSwitch = true;
            return false;
        }
        return true;
    }
    setParamBody() {
        let bodyParam = ::this.getParamBody();
    }
    getCenterContent() {
        return (
            <div>
                <header>tut</header>
                <sections>
                    <Link to ={GLOBAL.createPathLocale('about')}>about</Link>
                </sections>
            </div>
        );
    }
    getParamBody() {
        let {getParamsColumns} = GLOBAL;
        let headerParam = getParamsColumns('header');
        let LeftContainerParam = getParamsColumns();
        let CenterContainerParam = getParamsColumns();
        let RightContainerParam = getParamsColumns();
        let FooterParam = getParamsColumns('footer');
        headerParam.activeLink = this.pageName;
        CenterContainerParam.child = ::this.getCenterContent();
        FooterParam.activeLink = this.pageName;
        return {
            headerParam,
            LeftContainerParam,
            CenterContainerParam,
            RightContainerParam,
            FooterParam
        };
    }
    render() {
        let paramsBody = ::this.getParamBody();
        let {locale, switcherMenu} = this.props;
        if (is.undefined(locale) || !locale ) {
            let {getLocalePage} = GLOBAL;
            locale = getLocalePage('HomePageLocale');
        }
        return (
            !switcherMenu
            ?
                <div className="containerPage">
                    <BodyContainer
                        {...paramsBody}
                    />
                </div>
            :
                <div>
                    <p>
                        animate menu HOME
                    </p>
                </div>
        );
    }
}

HomePage.PropTypes = {
    listMenu: PropTypes.array.isRequired
};

function mapStateToProps(state) {
    return {
        locale: state.langPage.HomePageLocale,
        switcherMenu: state.switcherMenu
    };
}
export default connect(mapStateToProps)(HomePage);
