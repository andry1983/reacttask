import React from 'react';
import ReactDom from 'react-dom';
import configureStore from './store';
import { Provider } from 'react-redux';
import { BrowserRouter as Router} from 'react-router-dom';
import routes from './routes';
const store = configureStore();

ReactDom.render(
    <Provider store={store}>
        <Router>
            {routes}
        </Router>
    </Provider>,
    document.getElementById('apps')
);
