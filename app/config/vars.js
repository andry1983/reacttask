export let HomeStart = 'Home';  // яка сторінка стартова після програшу анімашки
export const LacalesList = ['en', 'ua'];
export const LANG_DEFAULT = LacalesList[1];
export const ANIMATE_TIME_CHANGE_PAGE = 1000;// ms


let shablon = {
    visability: true,
    child: null
};

let headerParam = {
    ...shablon,
    iconStatus: true,
    iconStyle: 'standart',
    menuStatus: true,
    activeLink: ''
};
let FooterParam = {
    ...shablon,
    menuStatus: false,
    activeLink: ''
};

export let getParamsColumns = (nameColumn) => {
    switch (nameColumn) {
        case 'header':
            return headerParam;
        case 'footer':
            return FooterParam;
        default:
            return {...shablon};
    }
};


