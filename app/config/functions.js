import is from 'is_js';
import * as Const from  '../const/PageInfo';
import {LacalesList, LANG_DEFAULT} from './vars';
import  Locales from  '../locale/index';

let changePageName = (dispatch, pageName)=>{
    if
    (
        dispatch &&
        is.not.undefined(dispatch) &&
        is.function(dispatch) &&
        is.string(pageName) &&
        is.not.empty(pageName)
    )
    {
        return dispatch({type: Const.CHANGE_PAGE, pageName});
    }
    return false;
};

let localeSwitch = (dispatch, lang) =>{
    if
    (
        dispatch &&
        is.not.undefined(dispatch) &&
        is.function(dispatch) &&
        is.string(lang) &&
        is.not.empty(lang)
    )
    {
        return dispatch({type: Const.CHANGE_LANG, lang});
    }
    return false;
};

let getLangurdgeProps = (props)=>{
    if (
        props &&
        is.not.undefined(props) &&
        is.not.empty(props) &&
        props.hasOwnProperty('match')
    )
    {
        let {lang} = props.match.params;
        return (is.inArray(lang, LacalesList)) ? lang : LANG_DEFAULT;
    }

    return LANG_DEFAULT;
};


let getPathProps = (props, param)=>{
    if (
        props &&
        is.not.undefined(props) &&
        is.not.empty(props) &&
        props.hasOwnProperty('match')
    )
    {
        let {path} = props.match;
        path = (path.split('/'));
        path = path[(path.length - 1)];
        return (is.not.undefined(path) && is.not.empty(path)) ? path : '/';
    }
    return '/';
};

let getPageName = (props)=>{
    if (
        props &&
        is.not.undefined(props) &&
        is.not.empty(props) &&
        props.hasOwnProperty('match')
    )
    {
        let {url} = props.match;
        url = (url.split('/'));
        let pageName = url.length > 0 ?  url[2] : '';
        return (is.not.undefined(pageName) && is.not.empty(pageName)) ? pageName : '/';
    }
    return '/';
};

let getPathForUrl = (param)=>{
    let structureUrl = {
        lang: '',
        page: '',
        params: ''
    };
    let loc = (location.pathname.replace(/^\/|\/$/g, '')).split('/');
    let index = 0, indexPath = 1;
    if (is.not.empty(loc) && structureUrl.hasOwnProperty(param)) {
        let lang = loc[index];
        if (is.not.inArray(lang, LacalesList)) {
            indexPath = 0;
        }
        let path = loc[indexPath];
        let params = loc[(indexPath + 1)];
        structureUrl = {
            lang: ((is.not.undefined(lang) && is.not.empty(lang) && is.inArray(lang, LacalesList)) ? lang : LANG_DEFAULT),
            page: ((is.not.undefined(path) && is.not.empty(path)) ? path : ''),
            params: ((is.not.undefined(params) && is.not.empty(params)) ? params : '')
        };
        return structureUrl[param];
    }
    return false;
};

let testUrlForDefLang = () => {
    let params = (location.pathname.replace(/^\/|\/$/g, '')).split('/');
    return (is.inArray(LANG_DEFAULT, params));
};

let createPathLocale = (namePage)=>{
    let lang = getPathForUrl('lang');
    let path = '';
    if (is.undefined(namePage)) {
        let pathUrl = getPathForUrl('page');
        pathUrl = (pathUrl && is.not.undefined(pathUrl)) ? pathUrl : '';
        let paramUrl = getPathForUrl('params');
        paramUrl = (paramUrl && is.not.undefined(paramUrl)) ? paramUrl : '';
        path = pathUrl + ((paramUrl) ? `/${paramUrl}` : '');
    }
    if (is.not.undefined(lang) && is.not.empty(lang) && is.not.undefined(namePage)) {
        let ClearPageName = namePage.replace(/^\/|\/$/g, '');
        if (lang === LANG_DEFAULT) {
            path = !testUrlForDefLang() ? `/${ClearPageName}` : `/${lang}/${ClearPageName}`;
        } else {
            path = (is.inArray(lang, LacalesList)) ? `/${lang}/${ClearPageName}` : `/${ClearPageName}`;
        }
    }
    return path;
};

let getLocalePage = (namePage) =>{
    let lang = getPathForUrl('lang');
    if (
        is.not.undefined(namePage) &&
        is.not.undefined(lang) &&
        is.not.undefined(Locales[lang]))
    {
        return Locales[lang][namePage];
    }
    return null;
};

export  {
    changePageName, // зміна імя активної сторінки в сторі
    localeSwitch, // зміна location
    getLangurdgeProps, // зчитує мову з параматрів роутінга в пропсах
    getPathProps, // зчитує шлях
    createPathLocale, // сформувати урл  на сторінки з відповідною локалізацією
    getPathForUrl, // зчитуваня параметрів з урла
    testUrlForDefLang,
    getLocalePage // читає локалізації конкретної сторінки
};
