import * as GLOBAL_VARS from './vars';
import * as GLOBAL_FUNCTION from './functions';

export const GLOBAL = {
    ...GLOBAL_VARS,
    ...GLOBAL_FUNCTION
};
