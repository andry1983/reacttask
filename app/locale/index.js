import en from './en.json';
import ua from './ua.json';
import  HomePageLocale from '../pages/Home/locale';
import  AboutPageLocale from '../pages/About/locale';
import  ContactsPageLocale from '../pages/Contacts/locale';
import  ErrorsPageLocale from '../pages/404/locale';

export default {
    en: {
        ...en,
        HomePageLocale:HomePageLocale.en,
        AboutPageLocale:AboutPageLocale.en,
        ContactsPageLocale:ContactsPageLocale.en,
        ErrorsPageLocale:ErrorsPageLocale.en
    },
    ua: {
        ...ua,
        HomePageLocale:HomePageLocale.ua,
        AboutPageLocale:AboutPageLocale.ua,
        ContactsPageLocale:ContactsPageLocale.ua,
        ErrorsPageLocale:ErrorsPageLocale.ua
    }
};
