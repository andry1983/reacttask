import React, { Component} from 'react';
import PropTypes from 'prop-types';

export default class LeftContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let {visability, child} = this.props;
        return (
            visability
                ?
                <div id="LeftContainer">
                    <h1>LeftContainer</h1>
                    {child}
                </div>
                :
                null
        );
    }
}

LeftContainer.defaultProps = {
    visability: false,
    child : null
};


LeftContainer.PropTypes = {
    visability : PropTypes.bool.isRequired,
    child:PropTypes.object.isRequired
};

