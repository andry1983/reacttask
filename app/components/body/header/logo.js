import React, { Component} from 'react';
import PropTypes from 'prop-types';

export default class Logo extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="logoContainer">
                <h1>Logo</h1>
            </div>
        );
    }
}


Logo.PropTypes = {
    visability: PropTypes.bool.isRequired, //відображення
    icon:PropTypes.string.isRequired//іконка
};
