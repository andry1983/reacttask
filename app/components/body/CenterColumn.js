import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class CenterContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let {visability, child} = this.props;
        return (
            visability
                ?
                <div id="CenterContainer">
                    <h1>CenterContainer</h1>
                    {child}
                </div>
                :
                null
        );
    }
}

CenterContainer.defaultProps = {
    visability: false,
    child : null
};


CenterContainer.PropTypes = {
    visability: PropTypes.bool.isRequired,
    child: PropTypes.object.isRequired
};
