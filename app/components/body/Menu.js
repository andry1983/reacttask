import React, { Component} from 'react';

export default class Menu extends Component {
    constructor(props) {
        super(props);
        this.animateChange = this.animateChange.bind(this);
    }
    animateChange(linkActive) {
        let obj = linkActive.target;
        let {changePage} = this.props;
        let nameLink = obj.getAttribute('value');
        changePage(nameLink);
    }
    render() {
        return (
            <nav id="navContainer">
                <ul className="linkContainer">
                    <li
                        value='Home'
                        onClick={this.animateChange}
                    >
                        Home
                    </li>
                    <li
                        value = 'About'
                        onClick={this.animateChange}
                    >
                        About
                    </li>
                    <li
                        value = 'Contact'
                        onClick={this.animateChange}
                    >
                        Contact
                    </li>
                </ul>
            </nav>
        );
    }
}
