import React, { Component} from 'react';
import './style/index.scss';
import CenterContainer from './body/CenterColumn';
import LeftContainer from './body/LeftColumn';
import RightContainer from './body/RightColumn';
import Header from './body/header';
import Footer from './body/Footer';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as Const from  '../const/PageInfo';
import {GLOBAL} from '../config/index';

class BodyContainer extends Component {
    constructor(props) {
        super(props);
        this.changePage = this.changePage.bind(this);
    }

    calcPositionElement() {// розраховує розміра колонок (ліва права центр ) якщо якась з трьох скрита
        // console.log(this.props);
    }
    changePage(namePage) {
        let {dispatch} = this.props;
        let {ANIMATE_TIME_CHANGE_PAGE} = GLOBAL;
        setTimeout(()=>{dispatch({type: Const.SWITCH_MENU, swithcMenuList: false, namePage});}, ANIMATE_TIME_CHANGE_PAGE);
        return dispatch({type: Const.SWITCH_MENU, swithcMenuList: true, namePage});
    }
    render() {
        this.calcPositionElement();
        let {
            headerParam,
            LeftContainerParam,
            CenterContainerParam,
            RightContainerParam,
            FooterParam
        } = this.props;
        return (
            <div id="bodyContainer">
                <Header
                    {...Object.assign({}, {
                        changePage: this.changePage
                    }, headerParam)}
                />
                <LeftContainer
                    {...LeftContainerParam}
                />
                <CenterContainer
                    {...CenterContainerParam}
                />
                <RightContainer
                    {...RightContainerParam}
                />
                <Footer
                    {...FooterParam}
                />
            </div>
        );
    }
}


BodyContainer.defaultProps = {
    headerParam: {
        visability: false
    },
    LeftContainerParam: {
        visability: false
    },
    CenterContainerParam: {
        visability: false
    },
    RightContainerParam: {
        visability: false
    },
    FooterParam: {
        visability: false}
};

BodyContainer.PropTypes = {
    headerParam: PropTypes.object.isRequired,
    LeftContainerParam: PropTypes.object.isRequired,
    CenterContainerParam: PropTypes.object.isRequired,
    RightContainerParam: PropTypes.object.isRequired,
    FooterParam: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        switcherMenu: state.switcherMenu
    };
}

export default connect(mapStateToProps)(BodyContainer);
