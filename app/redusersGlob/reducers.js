import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import * as Const from  '../const/PageInfo';
import  Locales from  '../locale/index';

let initialState = {
    pageName: '',
    langPage: {},
    switcherMenu: false
};

function pageInfo(state = initialState, action) {
    switch (action.type) {
        case Const.CHANGE_PAGE: {
            let {pageName} = action;
            return {pageName};
        }
        default:
            return state;
    }
}
function localeInfo(state = initialState, action) {
    switch (action.type) {
        case Const.CHANGE_LANG: {
            let {lang} = action;
            return {...Locales[lang]};
        }
        default:
            return state;
    }
}
function swithcMenuList(state = initialState, action = {swithcMenuList: false}) {
    switch (action.type) {
        case Const.SWITCH_MENU: {
            return action.swithcMenuList;
        }
        default:
            return false;
    }
}

export default combineReducers({
    routing: routerReducer,
    pageInfo: pageInfo,  // ...AppReducer
    langPage: localeInfo,
    switcherMenu: swithcMenuList// перемиканя сторінок з анімацією
});
