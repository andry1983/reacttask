const imgPath = require('./const/imgPath');
module.exports = function(){
    return {
        module: {
            rules:
                [
                    {
                        test: /\.(jpe?g|png|svg)$/,
                        include:imgPath,
                        loader:'file-loader',
                        options:{
                            name: 'images/[name].[ext]'
                        }
                    }
                ]
        }
    };
};
