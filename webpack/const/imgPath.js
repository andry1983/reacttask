const path = require('path');
const static = require('./static');
const imgPath =  path.resolve(static, '/images');
module.exports = imgPath;
