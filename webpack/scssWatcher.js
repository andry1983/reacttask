const entry = require('./const/entry');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
module.exports = function(env){
    const isDev = env === "development";
    const extractSass = new ExtractTextPlugin({
        filename: "css/[name].css",
        disable: isDev
    });
    return {
        module: {
            rules:
                [
                    {
                        test: /\.scss$/,
                        include:entry,
                        use: !isDev
                            ?
                            extractSass.extract({
                                fallback:'style-loader',
                                use:
                                    [
                                        {
                                            loader: 'css-loader',
                                            options:{
                                                minimize:true
                                            }
                                        },
                                        {
                                            loader: 'sass-loader',
                                            options: {
                                                includePaths: [
                                                    path.resolve(entry, "../node_modules/compass-mixins/*"),
                                                    path.resolve(entry, "../node_modules/compass-mixins/lib"),
                                                    path.resolve(entry, "../sass/susy"),
                                                    path.resolve(entry, "../sass/configuration"),
                                                    path.resolve(entry, "../sass/myMixins")
                                                ]
                                            },
                                        }
                                    ]
                            })
                            :
                            [
                                {loader: 'style-loader'},
                                {loader: 'css-loader'},
                                {
                                    loader: 'sass-loader',
                                    options: {
                                        includePaths: [
                                            path.resolve(entry, "../node_modules/compass-mixins/*"),
                                            path.resolve(entry, "../node_modules/compass-mixins/lib"),
                                            path.resolve(entry, "../sass/susy"),
                                            path.resolve(entry, "../sass/myMixins")
                                        ]
                                    },
                                }
                            ]
                    }
                ]
        },
        plugins: (!isDev) ? [extractSass] : []

    };
};
